import React, { Component } from 'react';
import { graphql, gql } from 'react-apollo';
import PropTypes from 'prop-types';

import { GC_USER_ID } from '../constants';
import { ALL_LINKS_QUERY } from './LinkList';

class CreateLink extends Component {

  static propTypes = {
    history: PropTypes.object
  }

  state = {
    description: '',
    url: ''
  }

  handleChange = e => {
    this.setState({
      [e.target.name]: e.target.value
    });
  }

  handleClick = async () => {
    const postedById = localStorage.getItem(GC_USER_ID);
    if (!postedById) {
      console.log('No user logged it');
      return;
    }
    const { description, url } = this.state;
    await this.props.createLinkMutation({
      variables: {
        description,
        url,
        postedById
      },
      update: (store, { data: { createLink } }) => {
        const data = store.readQuery({ query: ALL_LINKS_QUERY });
        data.allLinks.splice(0,0,createLink);
        store.writeQuery({
          query: ALL_LINKS_QUERY,
          data
        });
      }
    });
    this.props.history.push(`/`);
  }

  render() {
    return (
      <div>
        <div className='flex flex-column mt3'>
          <input
            className='mb2'
            name='description'
            onChange={this.handleChange}
            placeholder='A description for the link'
            type='text'
            value={this.state.description}
          />
          <input
            className='mb2'
            name='url'
            onChange={this.handleChange}
            placeholder='The URL for the link'
            type='text'
            value={this.state.url}
          />
        </div>
        <button
          onClick={this.handleClick}
        >
          Submit
        </button>
      </div>
    );
  }
}

const CREATE_LINK_MUTATION = gql`
  # 2
  mutation CreateLinkMutation($description: String!, $url: String!, $postedById: ID!) {
    createLink(
      description: $description,
      url: $url,
      postedById: $postedById
    ) {
      id
      createdAt
      url
      description
      postedBy {
        id
        name
      }
      votes {
        id
        user {
          id
        }
      }
    }
  }
`;

export default graphql(CREATE_LINK_MUTATION, { name: 'createLinkMutation' })(CreateLink);
