import React, { Component } from 'react';
import { gql, graphql, compose } from 'react-apollo';

import { GC_USER_ID, GC_AUTH_TOKEN } from '../constants';

class Login extends Component {

  state = {
    login: true, // switch between Login and SignUp
    email: '',
    password: '',
    name: ''
  }

  handleConfirmClick = async () => {
    const { name, email, password, login } = this.state;
    const { signinUserMutation, createUserMutation, history } = this.props;
    let result = null;

    if (login) {
      result = await signinUserMutation({
        variables: {
          email,
          password
        }
      });
    } else {
      result = await createUserMutation({
        variables: {
          name,
          email,
          password
        }
      });
    }
    const id = result.data.signinUser.user.id;
    const token = result.data.signinUser.token;
    this._saveUserData(id, token);
    history.push('/');
  }

  _saveUserData = (id, token) => {
    localStorage.setItem(GC_USER_ID, id);
    localStorage.setItem(GC_AUTH_TOKEN, token);
  }

  handleChange = e => {
    this.setState({
      [e.target.name]: e.target.value
    });
  }

  render() {
    return (
      <div>
        <h4 className='mv3'>{this.state.login ? 'Login' : 'Sign Up'}</h4>
        <div className='flex flex-column'>
          {!this.state.login &&
            <input
              name='name'
              onChange={this.handleChange}
              placeholder='Your name'
              type='text'
              value={this.state.name}
            />}
          <input
            name='email'
            onChange={this.handleChange}
            placeholder='Your email address'
            type='text'
            value={this.state.email}
          />
          <input
            name='password'
            onChange={this.handleChange}
            placeholder='Choose a safe password'
            type='password'
            value={this.state.password}
          />
        </div>
        <div className='flex mt3'>
          <div
            className='pointer mr2 button'
            onClick={this.handleConfirmClick}
          >
            {this.state.login ? 'login' : 'create account' }
          </div>
          <div
            className='pointer button'
            onClick={() => this.setState({ login: !this.state.login })}
          >
            {this.state.login ? 'need to create an account?' : 'already have an account?'}
          </div>
        </div>
      </div>
    );
  }
}

const CREATE_USER_MUTATION = gql`
  mutation CreateUserMutation($name: String!, $email: String!, $password: String!) {
    createUser(
      name: $name,
      authProvider: {
        email: {
          email: $email,
          password: $password
        }
      }
    ) {
      id
    }

    signinUser(email: {
      email: $email,
      password: $password
    }) {
      token
      user {
        id
      }
    }
  }
`;

const SIGNIN_USER_MUTATION = gql`
  mutation SigninUserMutation($email: String!, $password: String!) {
    signinUser(email: {
      email: $email,
      password: $password
    }) {
      token
      user {
        id
      }
    }
  }
`;

export default compose(
  graphql(CREATE_USER_MUTATION, { name: 'createUserMutation' }),
  graphql(SIGNIN_USER_MUTATION, { name: 'signinUserMutation' })
)(Login);
