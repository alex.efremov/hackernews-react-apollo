import React from 'react';
import PropTypes from 'prop-types';
import Link from './Link';
import { graphql, gql } from 'react-apollo';

export const ALL_LINKS_QUERY = gql`
  query AllLinksQuery {
    allLinks {
      id
      createdAt
      url
      description
      postedBy {
        id
        name
      }
      votes {
        id
        user {
          id
        }
      }
    }
  }
`;

class LinkList extends React.Component {
  static propTypes = {
    allLinksQuery: PropTypes.object
  }

  _updateCacheAfterVote = (store, createVote, linkId) => {
    const data = store.readQuery({ query: ALL_LINKS_QUERY });
    const votedLink = data.allLinks.find(link => link.id === linkId);
    votedLink.votes = createVote.link.votes;
    store.writeQuery({ query: ALL_LINKS_QUERY, data });
  }

  render () {
    const { allLinksQuery } = this.props;
    if (allLinksQuery && allLinksQuery.loading) {
      return <div>Loading</div>;
    }

    if (allLinksQuery && allLinksQuery.error) {
      return <div>Error</div>;
    }

    const { allLinks } = allLinksQuery;

    return (
      <div>
        {allLinks.map((link, index) => (
          <Link
            index={index}
            key={link.id}
            link={link}
            updateStoreAfterVote={this._updateCacheAfterVote}
          />
        ))}
      </div>
    );
  }
};

export default graphql(ALL_LINKS_QUERY, { name: 'allLinksQuery' })(LinkList);
