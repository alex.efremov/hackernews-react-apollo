import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { gql, withApollo } from 'react-apollo';
import Link from './Link';

const ALL_LINKS_SEARCH_QUERY = gql`
  query AllLinksSearchQuery($searchText: String!) {
    allLinks(filter: {
      OR: [{
        url_contains: $searchText
      }, {
        description_contains: $searchText
      }]
    }) {
      id
      url
      description
      createdAt
      postedBy {
        id
        name
      }
      votes {
        id
        user {
          id
        }
      }
    }
  }
`;

class Search extends Component {
  static propTypes = {
    client: PropTypes.object
  }

  state = {
    links: [],
    searchText: ''
  }

  handleChange = e => {
    this.setState({
      [e.target.name]: e.target.value
    });
  }

  handleClick = async () => {
    const { searchText } = this.state;
    const result = await this.props.client.query({
      query: ALL_LINKS_SEARCH_QUERY,
      variables: { searchText }
    });
    const links = result.data.allLinks;
    this.setState({ links });
  }

  render() {
    return (
      <div>
        <div>
          Search
          <input
            name='searchText'
            onChange={this.handleChange}
            type='text'
          />
          <button
            onClick={this.handleClick}
          >
            OK
          </button>
        </div>
        {this.state.links.map((link, index) => (
          <Link
            index={index}
            key={link.id}
            link={link}
          />
        ))}
      </div>
    );
  }

}

export default withApollo(Search);
