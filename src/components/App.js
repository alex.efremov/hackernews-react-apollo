import React from 'react';
import '../styles/App.css';
import { Switch, Route } from 'react-router-dom';

import Header from './Header';
import LinkList from './LinkList';
import CreateLink from './CreateLink';
import Login from './Login';
import Search from './Search';

const App = () => (
  <div className='center w85'>
    <Header />
    <div className='ph3 pv1 background-gray'>
      <Switch>
        <Route
          component={Search}
          exact
          path='/search'
        />
        <Route
          component={CreateLink}
          exact
          path='/create'
        />
        <Route
          component={Login}
          exact
          path='/login'
        />
        <Route
          component={LinkList}
          exact
          path='/'
        />
      </Switch>
    </div>
  </div>
);

export default App;
